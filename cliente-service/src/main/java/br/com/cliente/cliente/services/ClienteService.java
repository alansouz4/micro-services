package br.com.cliente.cliente.services;

import br.com.cliente.cliente.exceptions.ClienteNotFoundException;
import br.com.cliente.cliente.gateways.ClienteGateway;
import br.com.cliente.cliente.models.Cliente;
import br.com.cliente.cliente.repositories.ClienteRespository;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ClienteService implements ClienteGateway {

    private final ClienteRespository clienteRespository;

    @Override
    public Cliente salvaCliente(Cliente cliente) {
        return clienteRespository.save(cliente);
    }

    @Override
    @NewSpan("consulta-id")
    public Cliente listaClientePorId(@SpanTag(key = "id") int id) {
        Optional<Cliente> cliente = clienteRespository.findById(id);

        if(!cliente.isPresent()){
            throw new ClienteNotFoundException();
        }

        return cliente.get();
    }
}
