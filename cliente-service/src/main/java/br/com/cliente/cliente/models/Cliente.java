package br.com.cliente.cliente.models;

//import br.com.cliente.security.principal.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

//    @Size(min = 4, max = 30, message = "Nome de ter no mínimo 4 caracteres e no máximo 30 caracteres")
    @NotNull(message = "Nome é obrigatório")
    private String nome;

//    @OneToOne
//    private Usuario usuario;
}
