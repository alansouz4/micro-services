package br.com.cliente.cliente.repositories;

import br.com.cliente.cliente.models.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClienteRespository extends JpaRepository<Cliente, Integer> {

    Optional<Cliente> findById(int clienteId);
}
