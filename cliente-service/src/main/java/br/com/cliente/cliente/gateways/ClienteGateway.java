package br.com.cliente.cliente.gateways;

import br.com.cliente.cliente.models.Cliente;

public interface ClienteGateway {

    Cliente salvaCliente(Cliente cliente);
    Cliente listaClientePorId(int id);
}
