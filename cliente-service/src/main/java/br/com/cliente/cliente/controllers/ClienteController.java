package br.com.cliente.cliente.controllers;

import br.com.cliente.cliente.models.Cliente;
import br.com.cliente.cliente.services.ClienteService;
import br.com.cliente.security.principal.Usuario;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
@RequiredArgsConstructor
public class ClienteController {

    private final ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente salvarCliente(@AuthenticationPrincipal Usuario usuario) {
        Cliente cliente = new Cliente();
        cliente.setNome(usuario.getName());
        return clienteService.salvaCliente(cliente);
    }

    @GetMapping("/{id}")
    public Cliente listarClientePorId(@AuthenticationPrincipal Usuario usuario, @PathVariable(name = "id")  int id){
        Cliente cliente = new Cliente();
        cliente.setId(usuario.getId());
        cliente.setNome(usuario.getName());
        return clienteService.listaClientePorId(id);
    }
}
