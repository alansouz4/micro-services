package br.com.cliente.cliente.models.dtos;

import lombok.Data;

@Data
public class CriaClienteRequest {

    private String nome;
}
