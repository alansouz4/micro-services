package br.com.cliente.security.principal;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Usuario {

    @Id
    private int id;
    private String name;
}
