package br.com.cartao.cartaoservice.cartao.models.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CartaoAtivarRequest {

    private String numero;

    @NotNull
    @JsonProperty("ativo")
    private Boolean ativo;
}
