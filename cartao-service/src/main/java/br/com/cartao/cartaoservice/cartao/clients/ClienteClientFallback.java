package br.com.cartao.cartaoservice.cartao.clients;

import br.com.cartao.cartaoservice.cartao.models.Cliente;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Cliente listarClientePorId(int id) {

//        Cliente cliente = new Cliente();
//        cliente.setNome("Alan Augusto");
//
//        return cliente;
        throw new ClienteOfflineException();
    }
}
