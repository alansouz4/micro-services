package br.com.cartao.cartaoservice.cartao.models.mappers;

import br.com.cartao.cartaoservice.cartao.models.Cartao;
import br.com.cartao.cartaoservice.cartao.models.request.CartaoAtivarRequest;
import br.com.cartao.cartaoservice.cartao.models.request.CartaoRequest;
import br.com.cartao.cartaoservice.cartao.models.response.CartaoAtivoReponse;
import br.com.cartao.cartaoservice.cartao.models.response.CartaoGetReponse;
import br.com.cartao.cartaoservice.cartao.models.response.CartaoResponse;
import br.com.cartao.cartaoservice.cartao.models.Cliente;
import br.com.cartao.cartaoservice.security.principal.Usuario;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao(CartaoRequest cartaoRequest) {

        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoRequest.getNumero());

        Cliente cliente = new Cliente();
        cliente.setId(cartaoRequest.getClienteId());

        cartao.setClienteId(cliente.getId());

        return cartao;
    }

    public CartaoResponse toCartaoResponse(Cartao cartao) {
        CartaoResponse cartaoResponse = new CartaoResponse();

        cartaoResponse.setId(cartao.getId());
        cartaoResponse.setNumero(cartao.getNumero());
        cartaoResponse.setClienteId(cartao.getClienteId());
        cartaoResponse.setAtivo(cartao.getAtivo());

        Usuario usuario = new Usuario();
        cartaoResponse.setDono(usuario.getName());

        return  cartaoResponse;
    }

    public Cartao toCartao(CartaoAtivarRequest cartaoAtivarRequest) {
        Cartao cartao = new Cartao();

        cartao.setNumero(cartaoAtivarRequest.getNumero());
        cartao.setAtivo(cartaoAtivarRequest.getAtivo());

        return cartao;
    }

    public CartaoAtivoReponse toCartaoAtivoResponse(Cartao cartao) {
        CartaoAtivoReponse cartaoAtivoReponse = new CartaoAtivoReponse();

        cartaoAtivoReponse.setId(cartao.getId());
        cartaoAtivoReponse.setNumero(cartao.getNumero());
        cartaoAtivoReponse.setClienteId(cartao.getClienteId());
        cartaoAtivoReponse.setAtivo(cartao.getAtivo());

        return cartaoAtivoReponse;
    }

    public CartaoGetReponse toCartaoGetReponse(Cartao cartao) {
        CartaoGetReponse cartaoGetReponse = new CartaoGetReponse();

        cartaoGetReponse.setId(cartao.getId());
        cartaoGetReponse.setNumero(cartao.getNumero());
        cartaoGetReponse.setClienteId(cartao.getClienteId());

        return cartaoGetReponse;
    }


}
