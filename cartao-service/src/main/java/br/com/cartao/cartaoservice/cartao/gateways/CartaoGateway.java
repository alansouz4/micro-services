package br.com.cartao.cartaoservice.cartao.gateways;

import br.com.cartao.cartaoservice.cartao.models.Cartao;

public interface CartaoGateway {

    Cartao criaCartao(Cartao cartao);
    Cartao buscarCartaoPorNumero(String numero);
    Cartao ativarCartao(Cartao cartao);
    Cartao buscarCartaoPorId(int id);
}
