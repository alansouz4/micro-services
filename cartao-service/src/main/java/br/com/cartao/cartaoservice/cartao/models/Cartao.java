package br.com.cartao.cartaoservice.cartao.models;

import br.com.cartao.cartaoservice.security.principal.Usuario;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Número cartão é obrigatório")
    @Column(unique = true)
    private String numero;

    private int clienteId;

    private String dono;

    @Column
    private Boolean ativo;
}
