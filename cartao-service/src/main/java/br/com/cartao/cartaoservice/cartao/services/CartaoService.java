package br.com.cartao.cartaoservice.cartao.services;

import br.com.cartao.cartaoservice.cartao.exception.CartaoNotFoundException;
import br.com.cartao.cartaoservice.cartao.clients.ClienteClient;
import br.com.cartao.cartaoservice.cartao.gateways.CartaoGateway;
import br.com.cartao.cartaoservice.cartao.models.Cartao;
import br.com.cartao.cartaoservice.cartao.models.Cliente;
import br.com.cartao.cartaoservice.cartao.repositories.CartaoRepository;
//import br.com.cartao.cartaoservice.cartao.services.ClienteService;
import br.com.cartao.cartaoservice.security.principal.Usuario;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CartaoService implements CartaoGateway {

    private final CartaoRepository cartaoRepository;
//    private final ClienteService clienteService;
    private final ClienteClient clienteClient;

    @Override
    public Cartao criaCartao(Cartao cartao){
        Cliente cliente = clienteClient.listarClientePorId(cartao.getClienteId());

        if (cliente != null) {

            cartao.setClienteId(cliente.getId());
            cartao.setAtivo(false);

//            Usuario usuario = new Usuario();
//            cliente.setNome(usuario.getName());

            return cartaoRepository.save(cartao);

        } else {
            throw new CartaoNotFoundException();
        }
    }

    @Override
    public Cartao buscarCartaoPorNumero(String numero) {
        Optional<Cartao> cartao = cartaoRepository.findByNumero(numero);

        if (cartao.isPresent()) {

            return cartao.get();
        }
        throw new CartaoNotFoundException();
    }

    @Override
    public Cartao ativarCartao(Cartao cartao) {

        Cartao databaseCartao = buscarCartaoPorNumero(cartao.getNumero());

        databaseCartao.setAtivo(cartao.getAtivo());

        return cartaoRepository.save(databaseCartao);

    }

    @Override
    public Cartao buscarCartaoPorId(int id) {

        Optional<Cartao> cartao = cartaoRepository.findById(id);

        if (cartao.isPresent()) {

            return cartao.get();
        }
        throw new CartaoNotFoundException();
    }
}
