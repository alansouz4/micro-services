package br.com.cartao.cartaoservice.cartao.models;

import lombok.Data;

@Data
public class Cliente {

    private int id;
    private String nome;
}
