package br.com.cartao.cartaoservice.cartao.controllers;

import br.com.cartao.cartaoservice.cartao.clients.ClienteClient;
import br.com.cartao.cartaoservice.cartao.models.Cartao;
import br.com.cartao.cartaoservice.cartao.models.mappers.CartaoMapper;
import br.com.cartao.cartaoservice.cartao.models.request.CartaoAtivarRequest;
import br.com.cartao.cartaoservice.cartao.models.request.CartaoRequest;
import br.com.cartao.cartaoservice.cartao.models.response.CartaoAtivoReponse;
import br.com.cartao.cartaoservice.cartao.models.response.CartaoGetReponse;
import br.com.cartao.cartaoservice.cartao.models.response.CartaoResponse;
import br.com.cartao.cartaoservice.cartao.services.CartaoService;
import br.com.cartao.cartaoservice.security.principal.Usuario;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
@RequiredArgsConstructor
public class CartaoController {

    private final CartaoService cartaoService;
    private final CartaoMapper cartaoMapper;
    private final ClienteClient clienteClient;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoResponse salvaCartao(@RequestBody @Valid CartaoRequest cartaoRequest,
                                      @AuthenticationPrincipal Usuario usuario) {

            Cartao cartao = cartaoMapper.toCartao(cartaoRequest);
//            cartao.setDono(usuario.getName());
            cartao = cartaoService.criaCartao(cartao);

        return cartaoMapper.toCartaoResponse(cartao);
    }

    @PatchMapping("/{numero}")
    public CartaoAtivoReponse ativarCartao(@PathVariable(name = "numero") String numero,
                                           @RequestBody CartaoAtivarRequest cartaoAtivarRequest){

            cartaoAtivarRequest.setNumero(numero);
            Cartao cartao = cartaoMapper.toCartao(cartaoAtivarRequest);

            cartao = cartaoService.ativarCartao(cartao);

            return cartaoMapper.toCartaoAtivoResponse(cartao);
    }

    @GetMapping("/{numero}")
    public CartaoGetReponse buscarCartaoPorNumero(@PathVariable(name = "numero") String numero) {

            Cartao cartao = cartaoService.buscarCartaoPorNumero(numero);

            return cartaoMapper.toCartaoGetReponse(cartao);
    }

    @GetMapping("id/{id}")
    public Cartao buscarCartaoPorId(@PathVariable(name = "id") int id,
                                    @AuthenticationPrincipal Usuario usuario) {
        Cartao cartao = new Cartao();
        cartao.setDono(usuario.getName());
        cartao.setClienteId(clienteClient.listarClientePorId(id).getId());

        return cartaoService.buscarCartaoPorId(id);
    }
}
