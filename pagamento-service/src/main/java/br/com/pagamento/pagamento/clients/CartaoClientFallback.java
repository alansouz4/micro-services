package br.com.pagamento.pagamento.clients;

import br.com.pagamento.pagamento.models.Cartao;

public class CartaoClientFallback implements CartaoClient {

    @Override
    public Cartao buscarCartaoPorId(int id) {

//        Cartao cartao = new Cartao();
//        cartao.setNumero("123456789");
//        cartao.setClienteId(1);
//        cartao.setAtivo(false);

//        return cartao;
        throw new CartaoOfflineException();
    }
}
