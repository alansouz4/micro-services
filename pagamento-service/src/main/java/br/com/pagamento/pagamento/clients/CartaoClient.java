package br.com.pagamento.pagamento.clients;

import br.com.pagamento.pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "cartao", configuration = CartaoClientConfigutarion.class)
public interface CartaoClient {

    @GetMapping("/cartao/id/{id}")
    Cartao buscarCartaoPorId(@PathVariable int id);
}
