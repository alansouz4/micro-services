package br.com.pagamento.pagamento.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "O cartao informado é inválido")
public class InvalidCartaoException extends RuntimeException {
}
