package br.com.pagamento.pagamento.models.mappers;

//import br.com.pagamento.cartao.models.Cartao;
import br.com.pagamento.pagamento.models.Cartao;
import br.com.pagamento.pagamento.models.Pagamento;
import br.com.pagamento.pagamento.models.requests.PagamentoRequest;
import br.com.pagamento.pagamento.models.responses.PagamentoResponse;
import br.com.pagamento.security.principal.Usuario;
import org.springframework.stereotype.Component;

@Component
public class PagamentoMapper {

    public Pagamento toPagamento(PagamentoRequest pagamentoRequest) {

        Cartao cartao = new Cartao();
        cartao.setId(pagamentoRequest.getCartao_id());

        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(pagamentoRequest.getDescricao());
        pagamento.setValor(pagamentoRequest.getValor());

        pagamento.setCartaoId(cartao.getId());

        return pagamento;
    }

    public PagamentoResponse toPagamentoResponse(Pagamento pagamento) {

        PagamentoResponse pagamentoResponse = new PagamentoResponse();

        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartao_id(pagamento.getCartaoId());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());

        Usuario usuario = new Usuario();
        pagamentoResponse.setDono(usuario.getName());

        return pagamentoResponse;
    }
    

}
