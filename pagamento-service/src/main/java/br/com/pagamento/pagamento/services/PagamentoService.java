package br.com.pagamento.pagamento.services;

//import br.com.pagamento.cartao.services.CartaoService;
import br.com.pagamento.pagamento.clients.CartaoClient;
import br.com.pagamento.pagamento.exception.PagamentoNotFoundException;
import br.com.pagamento.pagamento.gateways.PagamentoGateway;

import br.com.pagamento.pagamento.models.Cartao;
import br.com.pagamento.pagamento.models.Pagamento;
import br.com.pagamento.pagamento.respositories.PagamentoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PagamentoService implements PagamentoGateway {

    private final PagamentoRepository pagamentoRepository;
    private final CartaoClient cartaoClient;

    @Override
    public Pagamento realizarPagamento(Pagamento pagamento) {
        Cartao cartao = cartaoClient.buscarCartaoPorId(pagamento.getCartaoId());

        if(cartao != null) {
            pagamento.setCartaoId(cartao.getId());

            return pagamentoRepository.save(pagamento);
        }
        throw new PagamentoNotFoundException();
    }

    @Override
    public List<Pagamento> buscarPagamentosPorCartao(int cartaoId) {
        return pagamentoRepository.findAllByCartaoId(cartaoId);
    }


}
