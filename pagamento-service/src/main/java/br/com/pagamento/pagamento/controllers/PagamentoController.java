package br.com.pagamento.pagamento.controllers;

import br.com.pagamento.pagamento.models.Pagamento;
import br.com.pagamento.pagamento.models.mappers.PagamentoMapper;
import br.com.pagamento.pagamento.models.requests.PagamentoRequest;
import br.com.pagamento.pagamento.models.responses.PagamentoResponse;
import br.com.pagamento.pagamento.services.PagamentoService;
import br.com.pagamento.security.principal.Usuario;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class PagamentoController {

    private final PagamentoService pagamentoService;
    private final PagamentoMapper mapper;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoResponse salvarPagamento(@RequestBody @Valid PagamentoRequest pagamentoRequest,
                                             @AuthenticationPrincipal Usuario usuario) {

        Pagamento pagamento = mapper.toPagamento(pagamentoRequest);
        pagamento.setDono(usuario.getName());
        pagamento = pagamentoService.realizarPagamento(pagamento);

        return mapper.toPagamentoResponse(pagamento);
    }


    @GetMapping("/pagamentos/{id_cartao}")
    public List<Pagamento> listaPagamentoPorCartao(@PathVariable int id_cartao,
                                                   @AuthenticationPrincipal Usuario usuario) {
        return pagamentoService.buscarPagamentosPorCartao(id_cartao);
    }
}