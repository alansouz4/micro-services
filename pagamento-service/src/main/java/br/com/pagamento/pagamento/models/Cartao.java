package br.com.pagamento.pagamento.models;

import lombok.Data;

@Data
public class Cartao {

    private int id;
    private String numero;
    private int clienteId;
    private Boolean ativo;
}
