package br.com.pagamento.pagamento.respositories;

import br.com.pagamento.pagamento.models.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PagamentoRepository extends JpaRepository<Pagamento, Integer> {

    List<Pagamento> findAllByCartaoId(int cartaoId);
}
