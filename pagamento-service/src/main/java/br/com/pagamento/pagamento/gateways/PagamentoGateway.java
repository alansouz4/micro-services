package br.com.pagamento.pagamento.gateways;

import br.com.pagamento.pagamento.models.Pagamento;

import java.util.List;

public interface PagamentoGateway {

        Pagamento realizarPagamento(Pagamento pagamento);
        List<Pagamento> buscarPagamentosPorCartao(int id_cartao);
}
