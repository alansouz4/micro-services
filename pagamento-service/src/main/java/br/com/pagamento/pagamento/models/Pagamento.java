package br.com.pagamento.pagamento.models;

//import br.com.cartao.cartao.models.Cartao;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Cartão é obrigatório")
    private int cartaoId;

    @NotNull(message = "Descrição é obrigatório")
    private String descricao;

    @NotNull(message = "Valor é obrigatório")
    private BigDecimal valor;

    private String dono;
}
